## NUC8

### BISO基本设置：

- 先更新BIOS，荐升级到0071以上版本
- 高级-Boot-Secure Boot-去掉勾-关闭“安全启动”
- 高级-Boot-UEFI Boot勾选-开启UEFI
- 带Intel无线网卡、蓝牙→ 禁用
- 如果更换了免驱无线网卡/蓝牙→开启

### 包含如下的配置组合：

- 4K-config
- 1080P-config

### 更新日志：

2020-02-12:第一版

基于weachy（维奇）稍作修改，特此致谢！

