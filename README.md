## 前言

随着OpenCore日臻完善，将在以后会更多的用于黑苹果的安装。同时，在各位大佬的大力支持与推广，各种入门，进阶教程的推出，OpenCore已经从神界降临到人间。逐渐为普通黑苹果爱好者掌握和使用。OpenCore推出的初衷是简单易用，很多人对于里面的众多的参数的选择还是云里雾里，甚至一头懵逼。鉴于这个情况，独行秀才根据自己安装、使用OpenCore的一些配置，制作几类OpenCore-EFI模版，供广大黑苹果爱好者借鉴、使用。欢迎大家指正，共同完善，谢谢大家！（用Typora打开此格式文档.MD）

## 分类`（先简单分类，待以后完善）`

- 台式机
- 笔记本
- NUC8

### 台式机

- 老旧主板台式机（如H61、Z87、Z97等，3～5代U）
- 主流主板台式机（如B360、Z370、Z390等，6代以后的U）

### 笔记本

- 老旧CPU笔记本（一般是指3代、4代、5代U）
- 主流CPU笔记本（一般是6代以后的U）

### NUC8

CPU不同稍作微调即可。

## OpenCore-EFI模版（基于0.5.6）

### 模版内容

#### OpenCore-EFI基本结构：


![OpenCore-EFI基本结构](https://images.gitee.com/uploads/images/2020/0207/113717_67d54503_5656387.png "B2DF0C5.png")


#### OpenCore-EFI文件夹内容

BOOT包含BOOTx64.efi-OpenCore引导核心

OC包含：

- ACPI包含适合自己的DSDT与SSDT
- Drivers包含ApfsDriverLoader.efi、FwRuntimeServices.efi、VBoxHfs.efi等
- Kexts包含适合自己的驱动
- Tools包含modGRUBShell.efi、VerifyMsrE2.efi、CleanNvram.efi、Shell.efi等
- config.plist-OpenCore配置文件
- OpenCore.efi-OpenCore引导核心

### 反馈与完善

大家在使用模版的过程中，如果遇到问题，请截图，并与我联系，大家一起共同来解决问题，，来完善、充实OpenCore-EFI-配置模版。

### 更新日志

2020-02-12：添加NUC8模版

2020-02-06：第一版


特别是不能在远景论坛转载！可能被远景因此封号，谢谢合作

黑苹果OpenCore开放群，群号:9422866，注明“独行秀才Blog引入”

具体使用请参阅[OpenCore配置文字说明第三版（基于03月09日编译版）](https://shuiyunxc.gitee.io/2020/03/10/instru/index/)